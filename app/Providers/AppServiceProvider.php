<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootSocialite();
    }

    private function bootSocialite()
    {
        $socialite = $this->app->make('Laravel\Socialite\Contracts\Factory');
        $socialite->extend(
            'uclapi',
            function ($app) use ($socialite) {
                $config = $app['config']['services.uclapi'];
                return $socialite->buildProvider(UclApiProvider::class, $config);
            }
        );
    }
}
