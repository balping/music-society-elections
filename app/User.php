<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable {
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'uclid',
        'nickname',
        'department',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $visible = ['id', 'name'];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @param \Laravel\Socialite\Two\User $socialtieUser
     * @return \App\User|mixed
     */
    public static function updateOrCreateFromSocialiteUser(\Laravel\Socialite\Two\User $socialtieUser){
        return static::updateOrCreate([
            'email' => $socialtieUser->email,
        ], [
            'uclid' => $socialtieUser->id,
            'email' => $socialtieUser->email,
            'name' => $socialtieUser->name,
            'nickname' => $socialtieUser->nickname,
            'department' => $socialtieUser->user['department'],
        ]);
    }

    /**
     * Returns ids of positions that the user already voted for
     */
    public function alreadyVotedIds(){
        return \DB::table('user_votes')->where('user_id', $this->id)->pluck('position_id')->toArray();
    }


    public function alreadyVoted($position_id){
        return \DB::table('user_votes')->where('user_id', $this->id)->where('position_id', $position_id)->count() != 0;
    }

    public function messages(){
        return $this->hasMany(Message::class);
    }
}
