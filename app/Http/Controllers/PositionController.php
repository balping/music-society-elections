<?php

namespace App\Http\Controllers;

use App\Position;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\Rule;

class PositionController extends Controller {

    public function __construct(){
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $positions = Position::all();
        $votedIds = \Auth::user()->alreadyVotedIds();
        return view('positions.index', compact('positions', 'votedIds'));
    }


    /**
     * Display the specified resource.
     *
     * @param \App\Position $position
     * @return \Illuminate\Http\Response
     */
    public function show(Position $position){
        if(!$position->open || \Auth::user()->alreadyVoted($position->id)){
            abort(403);
        }

        return view('positions.show', compact('position'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param \App\Position $position
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Position $position){
        if(!$position->open || \Auth::user()->alreadyVoted($position->id)){
            abort(403);
        }

        $valid = $request->validate([
            'candidate.*' => [
                'integer',
                Rule::exists('candidates', 'id')->where(function($query) use ($position){
                    $query->where('position_id', $position->id);
                })
            ]
        ]);

        $candidates = $valid['candidate'] ?? [];

        if(sizeof($candidates) == 0){
            return back()->withErrors(['Select at least one candidate']);
        }

        if(sizeof($candidates) > $position->number_of_votes){
            return back()->withErrors(['You can select maximum ' . $position->number_of_votes . ' candidates.']);
        }

        DB::transaction(function() use ($candidates, $position){
            foreach($candidates as $candidate_id){
                DB::statement("UPDATE candidates SET votes = votes + 1 WHERE id = " . $candidate_id);
            }
            DB::table("user_votes")->insert([
                'user_id' => \Auth::user()->id,
                'position_id' => $position->id,
            ]);
        });

        return redirect(action('PositionController@index'))->withSuccess('Your vote has been counted.');
    }

}
