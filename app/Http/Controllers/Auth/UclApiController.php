<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Laravel\Socialite\Two\InvalidStateException;
use Socialite;


class UclApiController extends Controller {
	/*
	|--------------------------------------------------------------------------
	| Login Controller
	|--------------------------------------------------------------------------
	|
	| This controller handles authenticating users for the application and
	| redirecting them to your home screen. The controller uses a trait
	| to conveniently provide its functionality to your applications.
	|
	*/

	//use AuthenticatesUsers;

	public function redirectToProvider(){
		return Socialite::with('uclapi')->redirect();
	}

	public function handleProviderCallback(){
		try{
			$user = Socialite::with('uclapi')->user();
		} catch (InvalidStateException $e){
			return redirect('/login')->with([
				'error' => 'Authentication by UCL API failed.'
			]);
		}

		$members = config('voting.members');

		if(!in_array($user->email, $members)) {
            return redirect('/login')->with([
                'error' => 'You are not eligible to vote.'
            ]);
        }

		$user = \App\User::updateOrCreateFromSocialiteUser($user);
		\Auth::login($user);
		\Log::info('User logged in via UCL API: ' . $user->name);
		return redirect('/agm')->with([
			'success' => 'Logged in with UCL API.'
		]);
	}

}
