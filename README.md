# Music soc voting system

The voting is anonymous. The system counts your vote and keeps track of the fact that you voted to avoid double voting,
but it doesn't link your vote to your name. The core logic can be found in `app/Http/Controllers/PositionController.php`
