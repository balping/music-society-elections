<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/agm');
});

Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
Route::post('login', 'Auth\LoginController@login');
Route::post('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('testlogin', function(){
    if(config('app.env') === 'local') {
        $u = \App\User::where('email', 'test@localhost')->first();

        Auth::login($u);

        return redirect('/');
    }

    abort(404);
});

Route::get('ucllogin', ['as' => 'ucllogin', 'uses' => 'Auth\UclApiController@redirectToProvider']);

Route::get('login_callback', ['as' => 'ucllogin_callback', 'uses' => 'Auth\UclApiController@handleProviderCallback']);

Route::resource('positions', 'PositionController')->middleware('auth');

Route::get('/agm', 'ChatsController@index');

Route::get('/messages', 'ChatsController@fetchMessages');
Route::post('/messages', 'ChatsController@sendMessage');

