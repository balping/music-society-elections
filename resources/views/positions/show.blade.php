@extends('layouts.public')

@section('title', $position->name)

<?php
/** @var \App\Position $position */
/** @var \App\Candidate $candidate */
?>

@section('content')
    <section class="section">
        <div class="container">
            <form action="{{action('PositionController@update', $position)}}" method="POST">
                @method('PATCH')
                @csrf
                <div class="content">
                    <p>Please select up to {{$position->number_of_votes}} candidates.</p>
                    <p>Once you submit the form, your vote cannot be changed.</p>
                </div>
                @foreach($position->candidates as $candidate)
                    <div class="field">
                        <div class="control">
                            <label class="checkbox">
                                <input type="checkbox" name="candidate[]" value="{{$candidate->id}}">
                                {{$candidate->name}}
                            </label>
                        </div>
                    </div>
                @endforeach
                <div class="field is-grouped">
                    <div class="control">
                        <button class="button is-link" type="submit">Submit</button>
                    </div>
                    <div class="control">
                        <a class="button is-link is-light" href="{{action('PositionController@index')}}">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection
