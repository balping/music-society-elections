@extends('layouts.public')

@section('title', 'Positions')

<?php
/** @var \App\Position $position */
?>

@section('content')
    <section class="section">
        <div class="container">
            <a href="{{action('ChatsController@index')}}" class="button is-link" style="margin-bottom: 1rem; margin-top: -3rem;">AGM Livestream here</a>
            @include('partials._info')

            @foreach($positions as $position)
                <div class="box">
                    <h4 class="title is-4">{{$position->name}}</h4>
                    @if(in_array($position->id, $votedIds))
                        You have already voted.
                    @else
                        @if($position->open)
                            <a href="{{action('PositionController@show', $position)}}" class="button is-primary">Vote</a>
                        @else
                            Voting closed
                        @endif
                    @endif
                </div>
            @endforeach
        </div>
    </section>
@endsection
