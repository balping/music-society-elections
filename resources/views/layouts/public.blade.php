@push('styles')
    <link href="https://cdn.jsdelivr.net/npm/bulma@0.8.0/css/bulma.min.css" rel="stylesheet">
@endpush
<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
@include('partials._html_head')
<body class="app">

<div id="root">
	<nav class="navbar">
		<div class="navbar-brand">
			<a class="navbar-item" href="/">
				UCL Music Society Elections
			</a>
			<div role="button" class="navbar-burger" data-target="navMenu" aria-label="menu" aria-expanded="false">
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
				<span aria-hidden="true"></span>
			</div>
		</div>

		<div class="navbar-menu" id="navMenu">

            @component('components.navlink', ['href' => action('PositionController@index')])
                Voting
            @endcomponent
            @component('components.navlink', ['href' => action('ChatsController@index')])
                AGM Livestream
            @endcomponent
			@can('access-admin-panel')
				<div class="navbar-start">
					<a class="navbar-item" href="/admin">
						Admin panel
					</a>

				</div>
			@endcan

			<div class="navbar-end">
				@guest
					@component('components.navlink', ['href' => route('login')])
						Login
					@endcomponent
				@endguest
				@auth
					<div class="navbar-item has-dropdown is-hoverable ">
						<span class="navbar-link" style="cursor: pointer;">{{ Auth::user()->nickname }}</span>
						<div class="navbar-dropdown is-right" style="right: 5px;">
							@component('components.navlink', ['href' => route('logout'), 'attributes' => ['onclick' => 'event.preventDefault(); document.getElementById(\'logout-form\').submit();']])
								Logout
							@endcomponent

							<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
								{{ csrf_field() }}
							</form>
						</div>
					</div>
				@endauth
			</div>
		</div>
	</nav>



	<section class="hero">
        <div class="hero-body">
            <div class="container">
                <h1 class="title">
                    @yield('title')
                </h1>
            </div>
        </div>
    </section>


	@if(Session::has('error') || Session::has('success') || $errors->any())
	<section class="section">
		<div class="container">
			@if(Session::has('error'))
				<div class="notification is-danger">
					{{ Session::get('error') }}
				</div>
			@endif

			@if ($errors->any())
				<div class="notification is-danger">
					@foreach ($errors->all() as $error)
						<li>{{ $error }}</li>
					@endforeach
				</div>

			@endif

			@if(Session::has('success'))
				<div class="notification is-success">
					{{ Session::get('success') }}
				</div>
			@endif
		</div>
	</section>
	@endif


    <div id="content">
        @yield('content')
    </div>

</div>



<!-- Scripts -->
<script src="{{ asset('js/app.js') }}"></script>

@stack('scripts')

@yield('vue')
</body>
</html>
