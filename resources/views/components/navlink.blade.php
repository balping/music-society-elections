<a class="navbar-item{{ Request::fullUrlIs($href) ? ' is-active' : '' }}"
   href="{{ $href }}" <?php if(isset($attributes)){ foreach($attributes as $attr => $value){echo $attr . "=\"" . e($value) . "\"";} } ?> >
    {{$slot}}
</a>