<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield('title')@hasSection('title') | @endif{{ config('app.name') }}</title>

	<!-- Styles -->
	@stack('styles')

	<script>
		window.DS = {};
		DS.offset = (new Date({{ round(microtime(true) * 1000)  }})).getTime() - Date.now();
		DS.now = function(){
			return new Date(Date.now() + DS.offset);
		};
	</script>
</head>
