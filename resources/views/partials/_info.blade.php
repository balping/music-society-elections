<div class="content">
    <p>Voting is anonymous. The system counts your vote and keeps track of the fact that you voted to avoid double
        voting,
        but it doesn't link your vote to your name. To keep the election as transparent as possible, you can check the
        source
        code of this voting system on <a target="_blank" href="https://gitlab.com/balping/music-society-elections">GitLab</a>.</p>
    <p>For president, treasurer, volunteering officer you can <a href="https://studentsunionucl.org/election/leadership-race-spring-2020" target="_blank">vote here.</a></p>
    <p>Speaches can be downloaded from <a href="https://uclmusicsociety.co.uk/storage/app/media/downloads/speaches.docx" download>here</a>.</p>
</div>
