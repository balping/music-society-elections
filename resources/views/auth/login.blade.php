@extends('layouts.public')

@section('content')
<section class="section">
    <div class="container">
        <div class="content">
            <h2 class="title">Welcome to UCL Music Society's Election System</h2>
            <p>
                To vote you, need to authenticate yourself using your UCL account.<br>
                If you encounter any problems, contact {!!   obfuscateEmail('it@uclmusicsociety.co.uk') !!}
            </p>

        </div>

        @include('partials._info')

        <a href="{{action('Auth\UclApiController@redirectToProvider')}}">
            <img src="/images/signin.png" alt="Login with UCL">
        </a>

    </div>
</section>

@endsection
