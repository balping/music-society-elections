@extends('layouts.public')

@section('title', 'AGM')

@section('content')
    <section class="section">
        <div class="columns is-desktop">
            <div class="column">
                <iframe width="560" height="315" src="https://www.youtube.com/embed/{{env('YT_STREAM')}}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"></iframe>
                <br><a href="{{action('PositionController@index')}}" target="_blank" class="button is-link">Vote here</a>
                <br>Speaches can be downloaded from <a href="https://uclmusicsociety.co.uk/storage/app/media/downloads/speaches.docx" download>here</a>.
            </div>
            <div class="column" id="app" style="min-width: 400px;">
                <chats :user="{{ auth()->user() }}"></chats>
            </div>
        </div>
    </section>
@endsection

@push("scripts")
    <script src="/js/chat.js"></script>
@endpush
