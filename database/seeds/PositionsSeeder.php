<?php

use Illuminate\Database\Seeder;

class PositionsSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(){
        \DB::table('positions')->insert(
            [[
                'name' => 'Vice President',
                'number_of_votes' => 1,
            ],
            [
                'name' => 'Opera Producer',
                'number_of_votes' => 1,
            ],
            [
                'name' => 'Concert Band Manager',
                'number_of_votes' => 2,
            ],
            [
                'name' => 'String Orchestra Manager',
                'number_of_votes' => 2,
            ],
            [
                'name' => 'Symphony Orchestra Manager',
                'number_of_votes' => 2,
            ],
            [
                'name' => 'Symphony Chorus Manager',
                'number_of_votes' => 2,
            ],
            [
                'name' => 'Chamber Choir Manager',
                'number_of_votes' => 1,
            ],
            [
                'name' => 'A Cappella Manager',
                'number_of_votes' => 2,
            ],
            [
                'name' => 'Social Secretary',
                'number_of_votes' => 1,
            ],
            [
                'name' => 'Publicity Officer',
                'number_of_votes' => 1,
            ]]
        );
    }
}
